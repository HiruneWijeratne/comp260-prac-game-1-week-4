﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

    public BeeMove beePrefab;
    public PlayerMove player;
    public PlayerMoveII playerII;
    public int nBees = 50;
    public Rect spawnRect;

    public float SpawnTimerMin = 10;
    public float SpawnTimerMax = 20;
    private float SpawnTimer = 0;

    void Start()
    {
        // defalt spawn range
        if (spawnRect.width == 0)
        {
            spawnRect.width = 10;
            spawnRect.height = 10;
        }
        for (int i = 0; i < nBees; i++)
        {
            // instantiate the bees
            BeeMove bee = Instantiate(beePrefab);

            // attach to this object in the hierarchy
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + i;

            // position the bees
            float x = spawnRect.xMin + Random.value * spawnRect.width * 2;
            float y = spawnRect.yMin + Random.value * spawnRect.height * 2;
            bee.transform.position = new Vector2(transform.position.x - spawnRect.width + x, transform.position.y - spawnRect.height + y);
                // bee.transform.localPosition = transform.position;
            bee.transform.localRotation = transform.rotation;
        }     
        
    }    void Update()
    {
        if (SpawnTimer <= 0)
        {
            SpawnTimer = Random.value * SpawnTimerMax * Time.deltaTime;
            if (SpawnTimer < SpawnTimerMin * Time.deltaTime)
            {
                SpawnTimer = SpawnTimerMin * Time.deltaTime;
            }
            SpawnBees(1);
        }
        SpawnTimer = SpawnTimer - 1.0f * Time.deltaTime;
    }

    public void SpawnBees(float nubBees)
    {
        for (int i = 0; i < nubBees; i++)
        {
            // instantiate the bees
            BeeMove bee = Instantiate(beePrefab);

            // attach to this object in the hierarchy
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + i;

            // position the bees
            float x = spawnRect.xMin + Random.value * spawnRect.width * 2;
            float y = spawnRect.yMin + Random.value * spawnRect.height * 2;
            bee.transform.position = new Vector2(transform.position.x - spawnRect.width + x, transform.position.y - spawnRect.height + y);
            // bee.transform.localPosition = transform.position;
            bee.transform.localRotation = transform.rotation;
        }
    }

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // BUG! the line below doesn’t work (Does now!)
            Vector2 v = (Vector2)child.position;
            if ( (v - centre).magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(transform.position.x - spawnRect.width, transform.position.y - spawnRect.height),
         new Vector2(transform.position.x + spawnRect.width, transform.position.y - spawnRect.height));
        Gizmos.DrawLine(
         new Vector2(transform.position.x + spawnRect.width, transform.position.y - spawnRect.height),
         new Vector2(transform.position.x + spawnRect.width, transform.position.y + spawnRect.height));
        Gizmos.DrawLine(
         new Vector2(transform.position.x + spawnRect.width, transform.position.y + spawnRect.height),
         new Vector2(transform.position.x - spawnRect.width, transform.position.y + spawnRect.height));
        Gizmos.DrawLine(
         new Vector2(transform.position.x - spawnRect.width, transform.position.y + spawnRect.height),
         new Vector2(transform.position.x - spawnRect.width, transform.position.y - spawnRect.height));
    }
}
