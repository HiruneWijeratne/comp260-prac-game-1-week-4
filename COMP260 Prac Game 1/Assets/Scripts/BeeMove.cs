﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    // private state
    private float speed;
    private float turnSpeed;
    private Transform target;
    private Transform PlayerI;
    private Transform PlayerII;
    private Vector2 heading;
    private bool isQuitting = false;
    // explosion state
    public ParticleSystem explosionPrefab;

    void Start()
    {
        // find the player to set the target
        PlayerMove p = FindObjectOfType<PlayerMove>();
        target = p.transform;
        PlayerI = p.transform;
        PlayerMoveII pII = FindObjectOfType<PlayerMoveII>();
        PlayerII = pII.transform;

        // set bee stats
        minSpeed = 4;
        maxSpeed = 12;
        minTurnSpeed = 10;
        maxTurnSpeed = 40;

        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);
        // set speed and turnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
        Random.value);
    }


    void Update()
    {
        // get the vector from the bee to the target
        Vector2 direction = target.position - transform.position;
        // get the vector from the bee to Players
        Vector2 direction2 = PlayerI.position - transform.position;
        Vector2 direction3 = PlayerII.position - transform.position;
        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;
        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);

        if (direction2.magnitude < direction3.magnitude)
        {
            target = PlayerI;
        }
        else
        {
            // Uncomment to add Player II back
            //target = PlayerII;
        }
    }

    void OnApplicationQuit()
    {
        isQuitting = true;
    }

    void OnDestroy()
    {
        if (isQuitting == false)
        {
            // create an explosion at the bee's current position
            ParticleSystem explosion = Instantiate(explosionPrefab);
            explosion.transform.position = transform.position;
            // destroy the particle system when it is over
#pragma warning disable CS0618 // Type or member is obsolete
            Destroy(explosion.gameObject, explosion.duration);
#pragma warning restore CS0618 // Type or member is obsolete
        }       
    }


    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }

}
